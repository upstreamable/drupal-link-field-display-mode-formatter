# Link field display mode formatter

## Features

* Adds a formatter for link fields that displays the current entity with another view mode inside the link.
* It can be configured to display the rendered fields as inline (CSS: display: inline)

## Use case

It was developed to be used with [Menu item content fields](https://www.drupal.org/project/menu_item_fields)
to show an icon along the menu item.

## Notes

The formatter doesn't control when you select the same display mode that you are configuring
but it does have protection for recursive rendering just like the entity reference formatter from core.

## Similar modules

[Field group link](https://www.drupal.org/project/field_group_link) can achieve similar results
but this module allows to organize the display in different view modes so you can repeat fields
and reuse display modes all implememented as a formatter with no extra dependencies.

## Contributions

Patches on drupal.org are accepted but merge requests on
[gitlab](https://gitlab.com/upstreamable/drupal-link-field-display-mode-formatter) are preferred.
